using Demo1;
using Xunit.Abstractions;

namespace Demo2
{
    public class UnitTest1
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public UnitTest1(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public void Test_GetProducts()
        {
            try
            {



                int num = 1;
                var service = new ProductsTestService();

                var enumerable = service.GetProducts(num);

                if (enumerable.Count() == 0)
                {
                    _testOutputHelper.WriteLine("FAILED");
                    Assert.Fail("No products found");
                }
                _testOutputHelper.WriteLine("PASSED");
                Assert.True(true, " products found");
            }
            catch (Exception e)
            {
                _testOutputHelper.WriteLine(e.ToString());
                Assert.Fail("No products found");
            }

        }
    }
}