﻿namespace Demo1
{
    public class ProductsTestService
    {
        public IEnumerable<Product> GetProducts(int num = 0)
        {
            if (num == 0) return ArraySegment<Product>.Empty;

            return new Product[]
            {
                new Product()
                {
                    Category = "asa",
                    Id = new Guid(),
                    Name = "asas",
                    Price = 12
                }
            };
        }
    }
}
